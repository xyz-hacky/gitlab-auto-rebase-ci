#!/bin/bash

GITLAB_BASE_URL="${CI_API_V4_URL}/projects"

get_params="state=opened"

if [[ -n "$GITLAB_LABEL" ]]
then 
    get_params="${get_params}&labels=${GITLAB_LABEL}"
fi

opened_merge_requests=$(curl -H "Authorization: Bearer $GITLAB_ACCESS_TOKEN" $GITLAB_BASE_URL/"$CI_PROJECT_ID"/merge_requests?"$get_params")

for iid in $(echo "$opened_merge_requests" | jq '.[] | .iid'); do
  curl -X PUT -H "Authorization: Bearer $GITLAB_ACCESS_TOKEN" $GITLAB_BASE_URL/"$CI_PROJECT_ID"/merge_requests/"$iid"/rebase
done
