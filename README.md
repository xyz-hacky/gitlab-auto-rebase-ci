# GitLab Auto Rebase

This project was using GitLab API v4 and GitLab CI

## Installation

1. Copy the `scripts` folder and the `.gitlab-ci.yml` file.
2. Paste the files in your projects root directory.
3. Set up an [access token](https://gitlab.com/-/profile/personal_access_tokens) with `api` scope for `GITLAB_ACCESS_TOKEN`.![screenshot](/screenshots/token.png)
4. Set up a [project label](https://docs.gitlab.com/ee/user/project/labels.html) to speicfy the MR for rebasing (optional step).![screenshot](/screenshots/label.png)
5. Add `GITLAB_ACCESS_TOKEN`, `REBASE_TARGET_BRANCH` and `GITLAB_LABEL` (optional variable) in project's variables.![screenshot](/screenshots/variables.png)
6. **Done!** Your MR with `GITLAB_LABEL` will rebase `REBASE_TARGET_BRANCH` automatically.

## Reference
[Medium: How to automatically rebase all your Merge Requests on GitLab when pushing on master](https://medium.com/ovrsea/how-to-automatically-rebase-all-your-merge-requests-on-gitlab-when-pushing-on-master-9b7c5119ac5f)

## License
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
